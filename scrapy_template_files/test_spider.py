# -*- coding: utf-8 -*-
from scrapy.utils.test import get_crawler
from scrapy_tdd import *
import pytest

from XYZSpider import Spider

import os

# ATTENTION: this requires the pytest and pytest-describe package. Otherwise you'll get a "0 tests found"


def response_from(file_name):
    return mock_response_from_sample_file(my_path(__file__) + "/samples", file_name)

def describe_profile_spider():

    to_test = XYZSpider.from_crawler(get_crawler())

    def describe_data_extraction():
        resp = response_from("Sample_File.html")
        results = to_test.parse_profile(resp)

        def should_return_only_one_item():
            # parse results should hold only one item (sometimes parsers have to return a mix!)
            assert count_requests_in_parse_result(results) == 0
            assert count_items_in_parse_result(results) == 1

        def should_work_with_profile_example():
            item = results
            assert item["address"] == [u'Los Angeles, CA US 90210']
            assert item["specialties"] == u'Group Fitness, Aquatic Fitness, Functional Training, Kickboxing and Boxing, Martial Arts and MMA, Personal and Small Group Training, Endurance Sports, Home-based, Special Populations and Conditions, Injury Prevention and Post Rehab, Pulmonary Disease / Asthma, Specialized Equipment Training, Suspension Training'
            assert item["website"] == [u'http://www.tokufit.com']
            assert item["name"] == u'Colleen Evans'
            assert item["phone"] ==  [u'(323) 517-5907']

        def should_work_with_another_profile():
            req = request_from("StephanieKagel.html")
            item = to_test.parse_profile(req)

            assert item["address"] == [u'Los Angeles, CA US 90036-3337']
            assert item["specialties"] == u'Mind-Body, Nutrition, Weight Loss / Weight Management, Personal and Small Group Training'
            assert item["website"] == [u'http://www.stephaniestraining.com']
            assert item["name"] == u'Stephanie Kagel'
            assert item["phone"] ==  [u'(323) 216-0606']


    def describe_link_extraction_through_rules():
        resp = response_from("Listing_Sample_file.html")
        results = list(to_test.parse(resp))
        urls = urls_from_requests(results)

        def should_return_only_requests():
            assert 85 == len(results)
            # should only hold requests
            assert count_requests_in_parse_result(results) == 85
            assert count_items_in_parse_result(results) == 0

        def should_return_one_request_with_pagination_urls():
            assert "http://fake_url.com/acefit/trainer_ACECertified_AlphabeticalIndex.aspx?lastInit=a&page=1" in urls
            assert count_urls_with("trainer_ACECertified_AlphabeticalIndex.aspx", urls) == 1
            assert count_urls_with("page=1", urls) == 1

        def should_return_requests_with_profile_urls():
            assert "http://fake_url.com/acefit/trainer_profile.aspx?acecp=dxv68x2" in urls
            assert count_urls_with("trainer_profile.aspx", urls) == 78

        def should_not_return_requests_with_invalid_urls():
            # also check for URLs that should not be there
            assert "http://fake_url.com/acefit/trainer_ACECertified_AlphabeticalIndex.aspx?lastInit=a&page=0" not in urls



