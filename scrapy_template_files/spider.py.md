# Usage #

1. Read the comments
2. Copy only those parts you will need, omit the others
3. Adjust them according to your needs
4. **IMPORTANT: adjust names.** These are examples and the names were
choosen for telling examples, not for reflecting what your code
will do in a clean code scraper. **Names Matters.**
5. If you find something important is missing, let me (Ruediger) know




# Imports

Not all of these imports are always necessary, so thin them out once
everything is running well.

```
# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.loader import ItemLoader
from scrapy.selector import Selector
import scraping_tools
import re
import json
import sys

#from <projectname>.items import <ItemName>
```

# Starting the Spider object

Subclassing from CrawlSpider is necessary if you want to use Crawling
rules (see below). Otherwise subclassing Spider is more elegant.

The **name** attribute contains the spider name that's used for calling
 the spider from command line, e.g. *scrapy crawl xyz_spider*

The **allowed_domains** attribute contains all domains that are allowed.
This helps our spider to stay focused on one site or a few target
sites and keeps it from following links to other sites. Usually it's a
 good idea to set this.

```
class XYZSpider(CrawlSpider):
    name = "xyz_spider"
    allowed_domains = ["xyz.com"]
```

# Start requests

Each scraper needs to know where to start. For this you can either
type in some hard coded **start_urls**. Or have the **start_requests**
method generate the request objects the scraper needs to get started.

The latter is usually preferable, if you have many or complex URLs or
you have to start with complex requests that aren't just plain GET
requests.

Note that **start_requests** takes precedence over **start_urls**

```
    def start_requests(self):
        requests = []
        for page in something:
            url = ...
            requests.append(self.make_requests_from_url(url))
        return requests

    start_urls = (
        'http://rea-in-ny.com/brokers',
        'http://rea-in-ny.com/brokers',
    )

```

# Crawling Rules

Scrapy's CrawlSpider has a feature called *Crawling Rules*

When to use:

- It's the prefered way for crawling sites, if the site structure
allows us to do so
- It can be used if all links on a site are urls in *a hrefs*
(no POST-requests, no forms, no javascript links etc)
- it requires some Regex knowledge

How to use:

- rules are executed in order, so make sure you move each rule into the
right place. Usually the best practice is to have the less specific,
more general rules first (for crawling category links,
"next page" links) and the most specific rules last
(e.g. for visiting profiles or detail pages)
- always copy & paste one or two example URLs as comment above
the rule ... this makes review and understanding much easier
- remember, you can use multiple regexes for 'allow' and 'deny' ...
e.g. allow=('regex1', 'regex2')

- IMPORTANT: when testing the scraper look out for weird links the
scraper might be following. This happens quite often, usually for
ads, rating links, login links, images etc that share a common pattern
with "good" links.

* Solution: either adjust the 'allow' part of the rule that picks them
up or add some additional patterns to 'deny'



```python
    rules = (
        # Rules for next page links
        # Example http://vaporsearchusa.com/page/2/?post_type=listing&mkey[0]=cats&mkey[1]=select_your_state&mkey[2]=post_city_id&mkey[3]=address&s=texas&t=0f3e6e64c0&relation=OR
        Rule(LinkExtractor(allow=(r'\/page\/\d+\/')), follow=True),

        # Rules for detail links
        # Example: http://vaporsearchusa.com/city/beaumont/listing/smoke-shack/
        Rule(LinkExtractor(allow=(r'\/city\/.*\/listing\/'), deny=(r'authentications', r'<\/p', r'\.jpg$') ),
             callback='parse_vape_listing', follow=True),
    )

```

# Parse methods

A spider's parse methods are the place where the data is extracted from
downloaded pages.

A spider can have as many parse methods as you need, but don't overdo
and give them meaningful names (no, *parse* is not a meaningful name,
unless there is only one parse method). A good convention is to name
all parse methods *'parse_<the page type it parses>'* e.g.
*'parse_profile_page'*

Note that the method 'parse' is called by default, if your Requests
don't have a callback set. Also the CrawlSpider has its own
implementation of 'parse' that's doing the crawling rule processing.

## Simple parse method structure:

```
    def parse_single_item_page(self, response):
        l = ItemLoader(XyzItem(), response)

        l.add_value("src", response.url)
        l.add_xpath('xytz', ".//i/text()")
        return l.load_item()
```


## Parsing multiple items per page

Another caveat: if there is a python crash within a parse method, any
results that might have been extracted already within the current
call to the method are lost.

That's no problem when there's only one record to extract (for example
when parsing a single profile page). But when parsing a whole listing
of 100 records it's deadly for data completeness if a parse method
crashes after successfully parsing 98 records, losing them all.

Solution for this: put the parsing of single items into a separate
method and put it int a try except block (as shown below).

```
    def parse_multi_item_page(self, response):
        results = []
        for entry in response.xpath("//table[@id='Table3']"):
            try:
                result = self.parse_single_item(sel, response)
                if result: results.append(result)
            except Exception, e:
                et, ei, tb = sys.exc_info()
                print et
                print ei
                print tb
        return results

    def parse_single_item(self, entry, response):
        l = ItemLoader(XyzItem(), selector = entry)

        l.add_value("src", response.url)
        l.add_xpath('xytz', ".//i/text()")
        return l.load_item()
```

# Handling forms

Webforms are usually based on sending special requests to the webserver.
These can be handled quite good with scrapy's FormRequest. But in some
cases you might also have to build a proper Request on your own.

Some things to keep in mind:

* it is almost always necessary to set the 'dont_filter' to True.
  Reason: the duplicate filter checks whether Requests with the same
  URL have been processed already and filters out any duplicates.
  Problem: POST-requests which are used for most forms usually have
  the same URL, no matter what's in the form data and so they would
  get filtered out after a first request for a form was processed
  successfully
* when a server isn't answering your requests, your request is most
  probably not 100% as the server expects, see below the
  'troubleshooting' section for some ideas what to do


Now examples for both situations.

## Handling Forms using FormRequest

- That's the Preferred method as this is a "most details already
  done for you" method
- IMPORTANT: it only works better than other approaches if you
  create it from a response object that contains the actual form.
  Means creating it with *FormRequest.from_response* is imperative!!
- it will scan the form in the response object and build a request
  that emulates this form
- if there are multiple forms on the page, you need to specify which
  form to use through the formid, formname, or formxpath param
- sometimes sites mix some javascript in their forms, in this case you
  might have to add some data manually to the request

- more details: https://doc.scrapy.org/en/latest/topics/request-response.html#formrequest-objects


```
    def build_form_request(self, response, some_form_data):
        req = FormRequest.from_response(response,
                                        formxpath="//form[@id='refine_form']", # this is only necessary when there
                                                                            # are multiple forms on the same page
                                        formdata=some_form_data, # the form data the scraper want's to send
                                        callback=self.parse_form_response)
        print req.headers # needed only for debugging requests that don't get the expected answer
        print req.body # needed only for debugging requests that don't get the expected answer
        return req
```

## Handling Forms using hand built requests

- Warning: this can result in dirty and not so nice code that
  requires lot of testing ... and should be avoided if possible
- but if FormRequest.from_response doesn't work right out of the box,
  and can't be adjusted to work, this is the best way to get this done
- the idea behind this approach: what FormRequest.from_response
  is doing automatically (and failing at) can be done by you much better
  manually (thanks to your insights and coder smarts)
- nonetheless in most cases it's still a good idea to use FormRequest
  as it does some encoding stuff under the hood that you don't have to
  handle manually
- approach in one sentence: you'll have to check the requests that
  are done in Firefox+Firebug or Chrome developer tools and then model
  their request headers and body into a request object


```
    def build_own_form_request(self, some_data):
        req = FormRequest("http://crm.internal/clients/filter/",
                           method="POST",
                           formdata={ # alot of form data
                               "assigned": "",
                               "casefile": "",
                               "condition": some_data,
                               "count": "",
                               "country": "",
                               "email": "",
                               "name": "",
                               "order_by": "",
                               "phone": "",
                               "source": "",
                               "status": "",
                               "stock": "",
                           },
                           headers={"X-Requested-With": "XMLHttpRequest",
                                    'X-Weirdo-Header': 'some weird header'},
                           callback=self.parse_form_response)
        print req.headers # needed only for debugging requests that don't get the expected answer
        print req.body # needed only for debugging requests that don't get the expected answer
        return req
```



## Form Troubleshooting

.... when your form-requests don't get answered by the server

- problem usually comes from some small and subtle differences between
  the request you are sending and what the server expects ... and
  usually these differences are either in the request headers or the
  request body
- so the fastest approach is to 'print request.body' and
  'print request.headers' in your scraper and then
  compare to what Firebug/Chrome developer tools shows

Typical sources for problems that you should look at
- missing or wrong cookies
- missing or wrong headers
- missing or wrong parameters in the body
- wrong body encoding (xform-encoded vs. JSON vs. some custom stuff)



# Handling AJAX / JSON


## Ajax Headers

Use these headers when doing requests to Ajax powered APIs that return json instead of html
You might have to fine tune this based on what you see in firebug (see the troubleshooting section
on Form Requests above)

```
    xhr_headers = { "Accept": "application/json, text/javascript, */*",
                              "Accept-Language": "en-US,en;q=0.5",
                              "Content-Type": "application/json; charset=utf-8",
                              "X-Requested-With": "XMLHttpRequest",}
```

This is how to build such a ajax request. You have to build your own
json_data dict based on what the server expects

```
    def build_ajax_request(self, some_data_you_define):
        json_data = {"Data": {"Data":some_data_you_define[1]}}
        return Request("http://someurl.com/somewhere/driver.asmx/SetTransType", method = "POST",
                          body = json.dumps(json_data), callback = self.parse_json_response,
                          headers = self.xhr_headers,
                          dont_filter = True)
```

This is an example how to parse a json response

```
    def parse_json_response(self, response):
        # Step 1: make sure we have clean json ... means we have to check whether it is and sometimes we have
        # also to do some string magic to clean it up ('it' is response.text or response.body)
        json_string = response.text

        # Step 2: parse the javascript
        as_json = json.loads(json_string)

        # Step 2.5 DEBUGGING: output the JSON in a nicely formatted way so you can check it in the logs and copy it
        # into your unit tests (you will write unit tests, don't you?)
        print(json.dumps(as_json, indent=4, sort_keys=True))

        # Step 3: access the json as you like and move it into an item
        l = ItemLoader(MyItem(), response)
        l.add_value("abc", as_json['abc'])

        return l.load_item()
```
