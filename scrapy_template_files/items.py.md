
## Basic Processing methods ##

The following methods help to do some basic cleanup of our data output pipeline: 

```
def remove_duplicates(in_list):
    already_seen = set()
    ret_list = []
    for elem in in_list:
        if not elem in already_seen:
            ret_list.append(elem)
        already_seen.add(elem)
    return ret_list


def strip_strings(in_list):
    return [ re.sub('\s+',' ',s).strip() for s in in_list]


def remove_emptys(in_list):
    return filter(len, filter(None, in_list))


class Remove(object):
    def __init__(self, removal_string):
        self.removal_string = removal_string

    def __call__(self, value):
        if len(value) == 0: return value
        return value.replace(self.removal_string, "").strip()

```



## Custom fields ##

Of course any custom field can be added to any item, too. But make sure
to set good output_processors

```
from scrapy.loader.processors import Join, MapCompose, Compose, TakeFirst

class CustomItem(scrapy.Item):
    # define the fields for your item here like:
    src = scrapy.Field()
    field1 = scrapy.Field()
    field2 = scrapy.Field(
        output_processor=Compose(
            strip_strings, remove_emptys, remove_duplicates, Join(' ')),
    )
```