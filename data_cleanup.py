# a template for a short pandas based script used for cleaning up, deduping and sorting scrapy results
# (ideally that's not necessary, but when are things ideal?)

import pandas as pd
import random

df = pd.read_csv("full_scrape_1.csv",
                 encoding="utf-8", # for proper handling of non ASCII characters
                 dtype=object) # for keeping everything a string. Otherwise zip codes will become floats like "90210.0"


df.dropna(axis='columns', how="all", inplace=True) # drop completely empty columns
df.drop_duplicates(subset=['business_name', 'business_type', 'email', 'street_address', 'zip', 'website'],
                   inplace=True) # drop duplicates, you need to adjust the column names in subset in a way, we don't
                                 # miss any dupes but don't remove any unique records

# check and fix emails, if necessary

# sort the columns
df = df[[u'business_name', u'business_type',
u'phone',
u'email',
u'street_address', u'city', u'zip', u'state',
u'website',
u'src',
       ]]


# writing everything to file
df.to_csv("full_clean_data.csv", index=False, encoding="utf-8")
df.to_excel("full_clean_data.xlsx", index=False) # for large datasets this might crash


# create a smaller sample file for review
sample_ix=random.sample(range(len(df)), 2000)
rand_sample = df.iloc[sample_ix,:]

rand_sample.to_csv("random_sample.csv", index=False, encoding="utf-8")
rand_sample.to_excel("random_sample.xlsx", index=False)

