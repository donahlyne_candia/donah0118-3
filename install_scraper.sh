#! /bin/bash

set -e

#echo "Updating packages"
#sudo apt update -y
#sudo apt upgrade -y

echo "Installing packages"
sudo apt-get install -y python python-dev python-pip python-setuptools python-wheel libxml2-dev libxslt1-dev zlib1g-dev libffi-dev libssl-dev

echo "Updating pip"
pip install --user --upgrade pip==9.0.3

echo "Installing pip packages"
pip install --user -r requirements.txt

