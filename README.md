# README #

**Important 1: This project contains template in .md (markdown) format. Means this
readme and some of the template files are best viewed in a markdown viewer ... or ... the bitbucket website.**

**Important 2: This readme is meant as explanation how to use these templates. 
 It should not be the part of the project that you are creating from this template. 
 So when using this template, replace this readme with documentation of the actual project.**

This repository contains a template used for developing a small (single) scraper project. 

## How to use it ##

This template is meant to be used for small, single scraper projects. Such
projects don't get their own repository. Instead, they are copied to 
the current small projects repository (e.g. 2018_projects).

### Steps for using this template: ###

1. copy all files in the main directory of the template to a new subdirectory in the repository (e.g. ```2018_projects/<current date in format: DD_MM_YYY>_<project name>/```)
2. change into this new subdirectory
3. initialize the scrapy project: ```scrapy startproject <project name> .``` (the "." is important for the scripts in this template to work)
4. generate the (first) spider: ```scrapy genspider <spider name> <domain>```
5. select and copy the template code that you need from the ```scrapy_template_files``` folder into the real files (please not that these are Markdown files for better readability, so only copy snippets):
    * [scrapy.cfg](scrapy_template_files/scrapy.cfg.md) - a template containing important snippet for the the main scrapy configuration file, needed for deploying to scrapyd servers
    * [settings.py](scrapy_template_files/settings.py.md) - a template containing various snippets for the settings file where all the fine tuning and proxy configuration is done
    * [spider.py](scrapy_template_files/spider.py.md) - various code templates for setting up a spider quickly
    * [items.py](scrapy_template_files/items.py.md) - various code templates for getting the items set up quickly
6. copy the following files in full from the ```scrapy_template_files/``` folder and
   adjust them as needed:
    * [test_spider.py](scrapy_template_files/test_spider.py) - a copy & paste unit test template for testing scrapy spiders ... copy it into the ```spider``` folder and rename it to ```test_<spider file name>.py```
    * [test_items.py](scrapy_template_files/test_items.py) - a copy & paste unit test template for testing scrapy items ... copy it next to your ```items.py```
7. Copy and adjust all the files listed under "Basic Elements" below

6. Cleanup:
    * remove the ```scrapy_template_files``` and ```instruction_files``` folder (if you had copied them accidentally)
    * replace the content of this README.md file with the setup_instructions_xx.md (use the ..._en.md file unless told otherwise, also please copy the complete markdown sourcecode)
    * adjust the contents of the new README.md file to the project (e.g. remove sections that are not needed, adjust spider names etc ...)
    * remove any uneeded files


## Basic Elements ##

This template contains all the basic elements a small, single scraper project should have:

* [requirements.txt](requirements.txt) - lists all python
  packages needed by this project. 
  
  Through ```pip install --user -r requirements.txt```
  all required packages can be installed. Keeping this file complete
  is especially important for projects that are deployed to servers
   and/or delivered to clients

* [pytest.ini](pytest.ini) - configuration for pytest that automatically avoids
  running all the tests in venv/ where usually the various libraries are installed

* [install_scraper.sh](install_scraper.sh) - a short linux script that installs
  the scraper and all its dependencies on a client's machine. For client use!

* [smoke_test_scraper.sh](smoke_test_scraper.sh) - a short linux script that tests
  an installed scraper and all its dependencies on a client's machine. For client use!

* [data_cleanup.py](data_cleanup.py) - a small script template that can be used 
  for loading scraping results and then doing cleanup or data enrichment operations (ideally this is not necessary)

* [Vagrant](Vagrant) - configuration a vagrant virtual box that emulates the
  client's environment and is used for testing the installation and test script 
  ( [install_scraper.sh](install_scraper.sh) and [smoke_test_scraper.sh](smoke_test_scraper.sh) ). Can be started through 
  ```vagrant up``` Requires installed vagrant 2.x.


## What's not here ##

As this is a small single spider project template, the following elements are not needed (and most of them not present) as they are not needed (although you might expect them):

* [fabfile.py](fabfile.py) and [deploy/](deploy/) - no separate fabric     deployment for small projects
  
* [bitbucket-pipelines.yml](bitbucket-pipeline.yml) - small projects don't have 
  their own pipeline, instead they are tested through the pipeline in the 
  overarching repository (e.g. 2018_projects)

* [scrapers](scrapers) - there's only one scraper, means there is no need for a "scrapers" folder

* [.gitignore](.gitignore) - is defined at the level of the overarching repository
  
  If you need to commit an ignored file or folder to git nonetheless,
  use git's -f (force) option

