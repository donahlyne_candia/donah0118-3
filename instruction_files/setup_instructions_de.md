# Installationsanleitung #
*(für Linux / Ubuntu)*

## Systemanforderungen: ##
- Python 2.7
- Scrapy 1.0 oder neuer - siehe: http://doc.scrapy.org/en/latest/intro/install.html
- Weitere Bibliotheken werden in der requirements.txt angegeben und können mit den u.g. Schritten installiert werden

Diese Anleitung wurde mit Ubuntu 16.04 LTS und Mint 19 getestet und funktioniert wahrscheinlich auf allen aktuellen Debian/Ubuntu basierten Distributionen. Das Scrapy Project läuft auch auf anderen Linux-Distributionen, Mac und Windows, jedoch weicht die Installation von den u.g. Schritten ab. 

### Installationsablauf ###
1. (optional) Potentielle Probleme mit "locale" auflösen (wenn von einem nicht-englischen System eingeloggt wird):

	> `sudo apt-get install language-pack-id`

	> `sudo dpkg-reconfigure locales`

2. In das Verzeichnis des Scrapy Projekts wechseln (wo die scrapy.cfg liegt)

3. Installationsscript starten, bei Bedarf die angefragten Eingaben tätigen:

	> `./install_scraper`

4. (optional) den Basis-("Smoke")-Test starten:

	> `./smoke_test_scraper.sh`

   Er sollte ohne Fehlermeldung durchlaufen und am Ende eine Statistik eines kurzen Scraper-Durchlaufs mit einem ```'item_scraped_count'```: von ca. 10 anzeigen

5. Öffnen sie die settings.py und passen sie die Einstellungen an

	* ```HTML_DIRECTORY``` 	Verzeichnis in das die HTML Dateien heruntergeladen werden

	* Für höhere Geschwindigkeit und bessere Tarnung werden Proxy-Server verwendet. Diese können deaktiviert werden, indem die Zeile ```'bundesanzeiger_de.middlewares.rotate_user_agent.RotateUserAgentMiddleware' :400,``` entfernt oder auskommentiert wird. 

	* Alternativ können die vorhandenen (funktionsfähigen) Proxy-Daten gegen eigene ausgetauscht werden (Dedicated Proxy-Server können z.B. unter https://blazingseollc.com/proxy/pricing/ gemietet werden)

	  ```PROXY_USER``` - Proxy-User
	  ```PROXY_PASSWORD``` - Proxy-Passwort
	  ```PROXY_SERVERS``` - eine Liste der zu verwendenden Proxy-IPs

## Nutzung ##
Sobald die benötigten Bibliotheken installiert wurden und die Settings angepasst wurden, kann Scrapy mit den folgenden Schritten gestartet werden:

In das Verzeichnis des Scrapy Projekts wechseln (wo die scrapy.cfg liegt) und dort ausführen: ```scrapy crawl bundesanzeiger -a input_html='<Pfad zur Tabelle im HTML-Format'```

	
Beispiel: ```scrapy crawl bundesanzeiger -a input_html='./erfassungsliste.AG.html'```

Dies startet den Download aller Jahresberichte.

Für Test und Fehlersuche können die Ergebnisse in verschiedenen Formaten ausgegeben werden, indem der Parameter ```-o <filename>``` an die Kommandozeile angefügt wird. Das Ausgabeformat wird anhand der Dateiendung bestimmt, unterstützte Formate sind: CSV (*.csv), JSON (*.js), JSON-Lines (*.jl), XML (*.xml)

